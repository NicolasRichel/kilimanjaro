/**
 * Kilimanjaro API client
 */
class ApiClient {

  constructor(baseUrl) {
    this.url = baseUrl;
    this.fetch = (path, options) => fetch(`${this.url}${path}`, options).then(res => res.json());

    this.get = (path) =>
      this.fetch(path);

    this.post = (path, body) =>
      this.fetch(path, { method: 'POST', headers: { 'Content-Type': 'application/json' }, body: JSON.stringify(body) });

    this.patch = (path, body) =>
      this.fetch(path, { method: 'PATCH', headers: { 'Content-Type': 'application/json' }, body: JSON.stringify(body) });

    this.delete = (path) =>
      this.fetch(path, { method: 'DELETE' });
  }

  // Labels API
  getLabels() {
    return this.get('/labels');
  }
  createLabel(label) {
    return this.post('/labels/create', label);
  }
  updateLabel(label) {
    return this.patch(`/labels/update/${label._id}`, label);
  }
  deleteLabel(id) {
    return this.delete(`/labels/delete/${id}`);
  }

  // Operations API
  getOperationsByPeriod(start, end) {
    return this.get(`/operations/by-period?start-date=${start}&end-date=${end}`);
  }
  getOperationByID(id) {
    return this.get(`/operations/${id}`);
  }
  createOperation(op) {
    return this.post('/operations/create', op);
  }
  updateOperation(op) {
    return this.patch(`/operations/update/${op._id}`, op);
  }
  deleteOperation(id) {
    return this.delete(`/operations/delete/${id}`);
  }
  updateOperationsSetLabel(label, operations) {
    return this.patch('/operations/bulk/set-label', { label, operations });
  }

};

export default new ApiClient(process.env.REACT_APP_API_URL);
