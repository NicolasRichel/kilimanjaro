import ApiClient from "./api-client.js";

function fetchAllLabels() {
  return ApiClient.getLabels();
}

function createLabel(label) {
  return ApiClient.createLabel(label); 
}

function updateLabel(label) {
  return ApiClient.updateLabel(label);
}

function deleteLabel(label) {
  return ApiClient.deleteLabel(label._id);
}

const service = {
  fetchAllLabels,
  createLabel,
  updateLabel,
  deleteLabel,
};

export default service;
