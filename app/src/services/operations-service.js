import ApiClient from "./api-client.js";

function fetchOperationsByPeriod([start, end]) {
  return ApiClient.getOperationsByPeriod(start, end);
}

function createOperation(operation) {
  return ApiClient.createOperation(operation);
}

function updateOperation(operation) {
  return ApiClient.updateOperation(operation);
}

function deleteOperation(operation) {
  return ApiClient.deleteOperation(operation._id);
}

function updateOperationsSetLabel(label, operations) {
  return ApiClient.updateOperationsSetLabel(
    label, operations.map(op => op._id)
  );
}

const service = {
  fetchOperationsByPeriod,
  createOperation,
  updateOperation,
  deleteOperation,
  updateOperationsSetLabel,
};

export default service;
