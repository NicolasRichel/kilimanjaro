import { createRoot } from "react-dom/client";
import App from "./App.js";

import "@/styles/global.scss";

const root = createRoot(document.getElementById("app-root"));
root.render(<App />);
