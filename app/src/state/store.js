import LabelsService from "@/services/labels-service.js";
import OperationService from "@/services/operations-service.js";

import {
  LOAD_LABELS,
  CREATE_LABEL,
  UPDATE_LABEL,
  DELETE_LABEL,

  CREATE_OPERATION,
  UPDATE_OPERATION,
  DELETE_OPERATION,
  UPDATE_OPERATIONS_SET_LABEL,

  SET_PERIOD,
  SET_DATE_RANGE,
} from "./actions.js";

let sub_id = 0;
const subscriptions = {
  labels: new Map(),
  operations:new Map(),
  period: new Map(),
  dateRange: new Map(),
};

const state = {
  labels: [],
  operations: [],
  period: [],
  dateRange: [],
};

const proxy = new Proxy(state, {
  set(target, prop) {
    const res = Reflect.set(...arguments);
    for (let callback of subscriptions[prop].values()) callback(target);
    return res;
  }
});

function subscribe(props, callback) {
  const subs = [];
  for (let prop of props) {
    subscriptions[prop].set(++sub_id, callback);
    subs.push([prop, sub_id]);
  }
  const unsubscribe = () => {
    for (let [prop, id] of subs) {
      subscriptions[prop].delete(id);
    }
  };
  return {
    state,
    unsubscribe
  };
}

function update(values) {
  Object.assign(proxy, values);
  return state;
}

async function dispatch(action, data) {
  switch (action) {
    case LOAD_LABELS: {
      const labels = await LabelsService.fetchAllLabels();
      update({ labels });
      break;
    }
    case CREATE_LABEL: {
      const label = await LabelsService.createLabel(data.label);
      const labels = state.labels.concat(label);
      update({ labels });
      break;
    }
    case UPDATE_LABEL: {
      const label = await LabelsService.updateLabel(data.label);
      const labels = state.labels.map(x => x._id === label._id ? label : x);
      update({ labels });
      break;
    }
    case DELETE_LABEL: {
      const label = await LabelsService.deleteLabel(data.label);
      const labels = state.labels.filter(x => x._id !== label._id);
      update({ labels });
      break;
    }

    case CREATE_OPERATION: {
      const op = await OperationService.createOperation(data.operation);
      const operations = state.operations.concat(op);
      update({ operations });
      break;
    }
    case UPDATE_OPERATION: {
      const op = await OperationService.updateOperation(data.operation);
      const operations = state.operations.map(x => x._id === op._id ? op : x);
      update({ operations });
      break;
    }
    case DELETE_OPERATION: {
      const op = await OperationService.deleteOperation(data.operation);
      const operations = state.operations.filter(x => x._id !== op._id);
      update({ operations });
      break;
    }
    case UPDATE_OPERATIONS_SET_LABEL: {
      await OperationService.updateOperationsSetLabel(data.label, data.operations);
      data.operations.forEach(op => op.labels.push(data.label));
      update({ operations: data.operations });
      break;
    }

    case SET_PERIOD: {
      const operations = await OperationService.fetchOperationsByPeriod(data.period);
      update({ operations, period: data.period });
      break;
    }
    case SET_DATE_RANGE: {
      update({ dateRange: data.dateRange });
      break;
    }

    default: {
      console.warn(`[Kilimanjaro] State - Unknown action dispatched: ${action}`);
    }
  }
}

export { subscribe, dispatch };
