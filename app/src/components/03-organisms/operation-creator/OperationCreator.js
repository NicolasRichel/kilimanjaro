import React from "react";
import { CREATE_OPERATION } from "@/state/actions.js";
import { dispatch } from "@/state/store.js";

// Components
import GenericContainer from "@generics/generic-container/GenericContainer.js";
import OperationForm from "@molecules/operation-form/OperationForm.js";

export default class OperationCreator extends React.Component {

  createOperation = (operation) => {
    dispatch(CREATE_OPERATION, { operation });
  };

  render() {
    return (
      <GenericContainer
        title="Créer des opérations"
        content={
          <OperationForm
            labels={this.props.labels}
            onSubmit={this.createOperation} />
        }
      />
    );
  }

}
