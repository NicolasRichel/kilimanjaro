import { getLastDayOfMonth, getMonthName, getMonth, getYear } from "@nrl/js-commons";
import React from "react";
import { SET_DATE_RANGE } from "@/state/actions.js";
import { dispatch, subscribe } from "@/state/store.js";

// Styles
import "./Timeline.scss";

function partitionPeriodByMonth(period) {
  if (period.length === 2) {
    let d = period[0], i = -1;
    const dateRanges = [];
    while (d < period[1]) {
      dateRanges.push([ d, getLastDayOfMonth(d) ]);
      let [ year, month ] = d.split('-').map(x => +x);
      if (month === 12) {
        year++;
        month = 1;
      } else {
        month++;
      }
      d = `${year}-${('0'+month).slice(-2)}-01`;
      i++;
    }
    dateRanges[i][1] = period[1];
    return dateRanges;
  }
}

export default class Timeline extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      period: [],
      dateRanges: [],
      selectedDateRange: 0
    };
  }

  componentDidMount() {
    const { state, unsubscribe } = subscribe(
      ["period", "dateRange"],
      s => this.setState({ period: s.period, dateRanges: partitionPeriodByMonth(s.period) })
    );
    this.unsubscribe = unsubscribe;
    this.setState({ period: state.period, dateRanges: partitionPeriodByMonth(state.period) });
  }

  componentWillUnmount() {
    this.unsubscribe();
  }

  selectDateRange(index, dateRange) {
    this.setState({ selectedDateRange: index });
    dispatch(SET_DATE_RANGE, { dateRange });
  }

  render() {
    const dateRanges = this.state.dateRanges?.slice().reverse() ?? [];
    return (
      <div className="Timeline">
        {dateRanges.map((dateRange, i) => {
          const date = dateRange[0];
          return (
            <div key={date}>
              <div
                className={`month-button ${this.state.selectedDateRange === i ? 'selected' : ''}`}
                onClick={() => this.selectDateRange(i, dateRange)}
              >
                {getMonthName(date)}
              </div>
              {getMonth(date) === '01' &&
                <div className="year-separator">
                  {getYear(date)} <span className="line"></span>
                </div>
              }
            </div>
          );
        })}
      </div>
    );
  }
}
