import React from "react";
import { openDialog } from "../dialog-viewport/dialog.js";

// Components
import ToolbarButton from "@atoms/toolbar-button/ToolbarButton.js";
import LabelsManager from "@organisms/labels-manager/LabelsManager.js";

// Styles
import "./Toolbar.scss";

export default class Toolbar extends React.Component {

  openLabelsManager = () => {
    openDialog({
      title: "Gestion des libellés", 
      component: <LabelsManager />
    });
  };

  render() {
    return (
      <div className="Toolbar">
        <ToolbarButton
          icon="tags"
          title="Gérer les libellés"
          onClick={this.openLabelsManager}
        />
      </div>
    );
  }
}
