import React from "react";
import { CREATE_LABEL, UPDATE_LABEL, DELETE_LABEL } from "@/state/actions.js";
import { dispatch, subscribe } from "@/state/store.js";

// Components
import GenericTable from "@/components/00-generics/generic-table/GenericTable.js";
import ActionButton from "@/components/01-atoms/action-button/ActionButton.js";
import LabelBadge from "@/components/01-atoms/label-badge/LabelBadge.js";
import LabelForm from "@/components/02-molecules/label-form/LabelForm.js";

// Styles
import "./LabelsManager.scss";

export default class LabelsManager extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      labels: [],
      currentLabel: {}
    };
  }

  componentDidMount() {
    const { state, unsubscribe } = subscribe(
      ["labels"], 
      ({ labels }) => this.setState({ labels })
    );
    this.unsubscribe = unsubscribe;
    this.setState({ labels: state.labels });
  }

  componentWillUnmount() {
    this.unsubscribe();
  }

  submitLabel = (label) => {
    dispatch(this.state.currentLabel._id ? UPDATE_LABEL : CREATE_LABEL, { label });
    this.setState({ currentLabel: {} })
  };

  deleteLabel = (label) => {
    dispatch(DELETE_LABEL, { label });
  };

  render() {
    return (
      <div className="LabelsManager">
        <div className="LabelsManager__table">
          <GenericTable
            rows={this.state.labels}
            rowKey="_id"
            columns= {[
              { id: "name", label: "Nom" },
              { id: "label", label: "Label", width: "200px" },
              { id: "actions", label: "Actions", width: "100px" },
            ]}
            cells={{
              label: label =>
                <LabelBadge label={label} />,
              actions: label =>
                <>
                  <ActionButton
                    margin="0 8px 0 0"
                    size="24" icon="pen"
                    onClick={() => this.setState({ currentLabel: label })}
                  />
                  <ActionButton
                    size="24" icon="trash"
                    onClick={() => this.deleteLabel(label)}
                  />
                </>
            }}
          />
        </div>
        <div className="LabelsManager__form">
          <LabelForm
            label={this.state.currentLabel}
            onSubmit={this.submitLabel}
            onClear={() => this.setState({ currentLabel: {} })}
          />
        </div>
      </div>
    );
  }

}
