import React from "react";
import { closeDialog, initDialog } from "./dialog.js";

// Components
import ActionButton from "@atoms/action-button/ActionButton.js";

// Styles
import "./DialogViewport.scss";

export default class DialogViewport extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      dialog: null
    };
  }

  componentDidMount() {
    initDialog(dialog => this.setState({ dialog }));
  }

  render() {
    const dialog = this.state.dialog;
    return (
      <div className={`DialogViewport ${dialog ? '' : 'hidden'}`}>
        {dialog &&
          <div className="dialog">
            <div className="dialog-header">
              <span className="title">
                {dialog.title}
              </span>
              <ActionButton
                margin="0 0 0 auto"
                size="30"
                icon="times"
                color="white"
                onClick={closeDialog}
              />
            </div>
            <div className="dialog-content">
              {dialog.component}
            </div>
            <div className="dialog-footer"></div>
          </div>
        }
      </div>
    );
  }

}
