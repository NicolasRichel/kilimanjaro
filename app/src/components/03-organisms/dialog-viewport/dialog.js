let callback = null;

function initDialog(fn) {
  if (!callback) {
    callback = fn;
  }
}

function openDialog(dialog) {
  callback?.(dialog);
}

function closeDialog() {
  callback?.(null);
}

export { initDialog, openDialog, closeDialog };
