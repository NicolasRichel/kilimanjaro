import React from "react";
import { initNotification } from "./notification.js";

// Styles
import "./NotificationViewport.scss";

export default class NotificationViewport extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      notification: {}
    };
  }

  componentDidMount() {
    initNotification(notification => {
      this.setState({ notification });
      setTimeout(() => this.setState({ notification: {} }), notification.timeout);
    });
  }

  render() {
    const notification = this.state.notification;
    return (
      <div className="NotificationViewport">
        {notification.message &&
          <div className={`notification ${notification.type}`}>
            {notification.message}
          </div>
        }
      </div>
    );
  }

}
