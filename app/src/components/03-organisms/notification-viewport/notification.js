let callback = null;

function initNotification(fn) {
  if (!callback) {
    callback = fn;
  }
}

function notify(notification) {
  callback?.(notification);
}

export { initNotification, notify };
