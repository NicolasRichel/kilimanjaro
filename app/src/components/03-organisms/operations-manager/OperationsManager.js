import React from "react";
import {
  CREATE_OPERATION,
  UPDATE_OPERATION,
  DELETE_OPERATION
} from "@/state/actions.js";
import { dispatch } from "@/state/store.js";

// Components
import GenericContainer from "@generics/generic-container/GenericContainer.js";
import GenericTable from "@generics/generic-table/GenericTable.js";
import ActionButton from "@atoms/action-button/ActionButton.js";
import AmountBox from "@atoms/amount-box/AmountBox.js";
import LabelBadge from "@atoms/label-badge/LabelBadge.js";
import OperationForm from "@molecules/operation-form/OperationForm.js";
import OperationsToolbar from "@molecules/operations-toolbar/OperationsToolbar.js";

export default class OperationsManager extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      currentOperation: {},
      selection: []
    };
  }

  create = (operation) => dispatch(CREATE_OPERATION, { operation });

  update = (operation) => dispatch(UPDATE_OPERATION, { operation });

  delete = (operation) => dispatch(DELETE_OPERATION, { operation });

  render() {
    const { labels, operations, dateRange } = this.props;
    const month = dateRange.length > 0 ? dateRange[0].slice(0, -3) : "";

    return (
      <GenericContainer
        title="Liste des opérations"
        content={
          <>
            <OperationForm
              labels={labels}
              month={month}
              onSubmit={this.create}
            />
            <OperationsToolbar
              labels={labels}
              operations={operations.filter(op => op.selected)}
            />
            <GenericTable
              rows={operations}
              rowKey="_id"
              columns={[
                { id: "checkbox", width: "28px" },
                { id: "date", label: "Date", width: "100px" },
                { id: "amount", label: "Montant", width: "80px" },
                { id: "reference", label: "Référence" },
                { id: "labels", label: "Labels", width: "200px" },
                { id: "actions", label: "Actions", width: "100px" },
              ]}
              cells={{
                checkbox: () =>
                  <input type="checkbox" />,
                amount: op =>
                  <AmountBox amount={op.amount} />,
                labels: op =>
                  labels.filter(l => op.labels.includes(l._id)).map(
                    label => <LabelBadge key={label._id} label={label} />
                  ),
                actions: op =>
                  <>
                    <ActionButton
                      margin="0 8px 0 0"
                      size="24" icon="pen"
                      onClick={() => this.setState({ currentOperation: op })}
                    />
                    <ActionButton
                      size="24" icon="trash"
                      onClick={() => this.delete(op)}
                    />
                  </>,
              }}
              rowOverlay={op => {
                if (this.state.currentOperation._id === op._id)
                  return (
                    <OperationForm
                      labels={labels}
                      operation={op}
                      onSubmit={this.update}
                      onClear={() => this.setState({ currentOperation: {} })}
                    />
                  );
              }}
            />
          </>
        }
      />
    );
  }

}
