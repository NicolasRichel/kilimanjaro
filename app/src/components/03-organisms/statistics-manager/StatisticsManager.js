import React from "react";
import StatisticsService from "@/services/statistics-service.js";

// Components
import GenericContainer from "@generics/generic-container/GenericContainer.js";
import AmountBox from "@atoms/amount-box/AmountBox.js";
import LabelsStatChart from "@molecules/labels-stat-chart/LabelsStatChart.js";
import TimeEvolutionStatChart from "@molecules/time-evolution-stat-chart/TimeEvolutionStatChart.js";

// Styles
import "./StatisticsManager.scss";

function StatisticsManager(props) {
  const total = StatisticsService._computeTotal(props.operations);
  return (
    <GenericContainer
      title="Statistiques"
      content={
        <div className="StatisticsManager">
          <div className="stats-container">
            <span>Total : </span><AmountBox amount={total} />
          </div>
          {props.operations.length > 0 && (
            <div className="chart-container">
              <LabelsStatChart
                operations={props.operations}
                labels={props.labels}
              />
              <TimeEvolutionStatChart
                operations={props.operations}
                dateRange={props.dateRange}
              />
            </div>
          )}
        </div>
      }
    />
  );
}

export default StatisticsManager;
