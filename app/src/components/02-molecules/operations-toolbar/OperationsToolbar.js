import React from "react";
import { UPDATE_OPERATIONS_SET_LABEL } from "@/state/actions.js";
import { dispatch } from "@/state/store.js";

// Components
import ActionButton from "@atoms/action-button/ActionButton.js";
import LabelsSelector from "@atoms/labels-selector/LabelsSelector.js";

export default class OperationsToolbar extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      label: null
    };
  }

  updateLabel() {
    if (this.state.label) {
      dispatch(UPDATE_OPERATIONS_SET_LABEL, {
        label: this.state.label,
        operations: this.props.operations
      });
      this.setState({ label: null });
    }
  }

  render() {
    const { labels, operations } = this.props;

    return (
      <div className="OperationsToolbar">
        {operations.length > 0 && (
          <div>
            <LabelsSelector
              labels={labels}
              value={this.state.label ? [this.state.label] : []}
              onChange={v => this.setState({ label: v[0] })}
            />
            <ActionButton
              icon="tags"
              onClick={() => this.updateLabel()}
            />
          </div>
        )}
      </div>
    );
  }

}
