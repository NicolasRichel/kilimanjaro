import React from "react";

// Components
import ColorInput from "@atoms/color-input/ColorInput.js";
import ActionButton from "@atoms/action-button/ActionButton.js";
import LabelBadge from "@atoms/label-badge/LabelBadge.js";
import StringInput from "@atoms/string-input/StringInput.js";

// Styles
import "./LabelForm.scss";

export default class LabelForm extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      label: props.label || {}
    };
  }

  componentDidUpdate() {
    if (this.props.label._id !== this.state.label._id) {
      this.setState({ label: this.props.label });
    }
  }

  setField = (field) => {
    return (value) => this.setState({
      label: {
        ...this.state.label,
        [field]: value
      }
    });
  };

  validate = () => {
    const { name, color, textColor } = this.state.label;
    return name && color && textColor;
  };

  submit = () => {
    if (this.validate()) {
      this.props.onSubmit?.(this.state.label);
    }
  };

  clear = () => {
    this.setState({ label: {} });
    this.props.onClear?.();
  };

  render() {
    const label = this.state.label;

    return (
      <div className="LabelForm">
        <div className="form-control">
          <label>Nom</label>
          <StringInput
            className="NameInput"
            value={label.name}
            onChange={this.setField('name')}
          />
        </div>
        <div className="form-control">
          <label>Couleur</label>
          <ColorInput
            value={label.color || "#CCCCCC"}
            onChange={this.setField('color')}
          />
        </div>
        <div className="form-control">
          <label>Couleur Texte</label>
          <ColorInput
            value={label.textColor}
            onChange={this.setField('textColor')}
          />
        </div>
        <div className="form-control">
          <span className="label-preview">
            <label>Label :</label>
            {label.name && <LabelBadge label={label} />}
          </span>
          <ActionButton
            size="30" icon="check"
            onClick={this.submit}
          />
          <ActionButton
            size="30" icon="times"
            onClick={this.clear}
          />
        </div>
      </div>
    );
  }

}
