import React from "react";

// Components
import ActionButton from "@atoms/action-button/ActionButton.js";
import AmountInput from "@atoms/amount-input/AmountInput.js";
import DateInput from "@atoms/date-input/DateInput.js";
import DayInput from "@atoms/day-input/DayInput.js";
import LabelsSelector from "@atoms/labels-selector/LabelsSelector.js";
import StringInput from "@atoms/string-input/StringInput.js";

// Styles
import "./OperationForm.scss";

export default class OperationForm extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      operation: props.operation || {}
    };
  }

  setField = (field) => {
    return (value) => this.setState({
      operation: {
        ...this.state.operation,
        [field]: value
      }
    });
  };

  validate = () => {
    const op = this.state.operation;
    return (!!op.date && op.amount > 0 && !!op.reference && op.labels.length === 1);
  };

  submit = () => {
    if (this.validate()) {
      this.props.onSubmit(this.state.operation);
      this.clear();
    }
  };

  clear = () => {
    this.setState({ operation: {} });
    this.props.onClear?.();
  };

  render() {
    const operation = this.state.operation;

    return (
      <div className="OperationForm">

      {this.props.month ? (
        <DayInput
          month={this.props.month}
          value={operation.date}
          onChange={this.setField('date')}
        />
      ) : (
        <DateInput
          value={operation.date}
          onChange={this.setField('date')}
        />
      )}

        <AmountInput
          value={operation.amount}
          onChange={this.setField('amount')}
        />

        <StringInput
          className="ReferenceInput"
          placeholder="Référence"
          value={operation.reference}
          onChange={this.setField('reference')}
        />

        <LabelsSelector
          labels={this.props.labels}
          value={operation.labels}
          onChange={this.setField('labels')}
        />

        <div className="actions">
          <ActionButton
            margin="0 8px 0 0"
            size="30" icon="check"
            onClick={this.submit}
          />
          <ActionButton
            size="30" icon="times"
            onClick={this.clear}
          />
        </div>

      </div>
    );
  }

}
