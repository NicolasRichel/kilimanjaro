import { getMonthDates, getDay } from "@nrl/js-commons";
import React from "react";
import { VictoryBar, VictoryChart, VictoryContainer, VictoryTheme } from "victory";
import StatisticsService from "@/services/statistics-service.js";

export default function TimeEvolutionStatChart(props) {
  const dates = props.dateRange ? getMonthDates(props.dateRange[0]) : [];
  const totalByDate = StatisticsService._computeTotalByDate(props.operations, dates);
  const data = Object.entries(totalByDate).map(([x, y]) => ({ x: getDay(x), y }));
  return (
    <div className="TimeEvolutionStatChart">
      <VictoryChart
        containerComponent={<VictoryContainer responsive={false} />}
        theme={VictoryTheme.material}
        width={700}
      >

        <VictoryBar
          style={{ data: { fill: '#444' } }}
          alignment="middle"
          data={data} 
        />

      </VictoryChart>
    </div>
  );
}
