// Styles
import "./GenericTable.scss";

export default function GenericTable(props) {
  const gridTemplateColumns = props.columns.map(col => col.width ?? "1fr").join(" ");

  return (
    <div className="generic-table">
      <div className="generic-table__head">
        <div
          className="generic-table__head__row"
          style={{ gridTemplateColumns }}
        >
          {props.columns.map(col =>
            <div className="generic-table__head__row__cell" key={col.id}>
              { col.label }
            </div>
          )}
        </div>
      </div>
      <div className="generic-table__body">
        {props.rows.map(row =>
          <div
            className="generic-table__body__row"
            key={row[props.rowKey]}
            style={{ gridTemplateColumns }}
          >
            {props.rowOverlay?.(row) &&
              <div className="generic-table__body__row__overlay">
                {props.rowOverlay?.(row)}
              </div>
            }
            {props.columns.map(col =>
              <div
                className="generic-table__body__row__cell"
                key={col.id}
                style={{ width: col.width }}
              >
                { props.cells[col.id]?.(row) ?? row[col.id] }
              </div>
            )}
          </div>
        )}
      </div>
    </div>
  );
}
