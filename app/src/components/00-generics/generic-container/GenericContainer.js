// Styles
import "./GenericContainer.scss";

export default function GenericContainer(props) {
  return (
    <div className="GenericContainer">
      {props.title &&
        <div className="GenericContainer__title">
          {props.title}
        </div>
      }
      <div className="GenericContainer__content">
        {props.content}
      </div>
    </div>
  );
}
