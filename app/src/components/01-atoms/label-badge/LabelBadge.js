// Styles
import "./LabelBadge.scss";

export default function LabelBadge({ label }) {
  return (
    <div
      className="LabelBadge"
      style={{
        backgroundColor: label.color || "#CCC",
        color: label.textColor || "#000"
      }}
    >
      {label.name}
    </div>
  );
}
