import React from "react";

// Components
import LabelBadge from "@atoms/label-badge/LabelBadge.js";

// Styles
import "./LabelsSelector.scss";

export default class LabelsSelector extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      hasFocus: false
    };
  }

  setValue = (label) => {
    return (e) => {
      const currentValue = this.props.value || [];
      let newValue = [];
      if (this.props.multi) {
        if (e.target.checked) {
          newValue = currentValue.concat(label._id);
        } else {
          newValue = currentValue.filter(id => id !== label._id);
        }
      } else {
        this.toggleFocus();
        if (e.target.checked) {
          newValue = [label._id];
        } else {
          newValue = [];
        }
      }
      this.props.onChange?.(newValue);
    }
  };

  toggleFocus = () => {
    this.setState({ hasFocus: !this.state.hasFocus });
  };

  render() {
    const value = this.props.value || [];
    return (
      <div className="LabelsSelector">
        <span
          className={`selection-container ${this.state.hasFocus ? "focus" : ""}`}
          onClick={this.toggleFocus}
        >
          {value.length === 0 ?
            <div className="placeholder">Sélectionner des libellés</div> :
            this.props.labels.filter(
              l => value.includes(l._id)
            ).map(label =>
              <LabelBadge key={label._id} label={label} />
            )
          }
        </span>
        <div
          className={`options-container ${this.state.hasFocus ? "" : "hidden"}`}
        >
          {this.props.labels.map(label =>
            <div key={label._id} className="option">
              <input type="checkbox"
                checked={value.includes(label._id)}
                onChange={this.setValue(label)}
              />
              <LabelBadge label={label} />
            </div>
          )}
        </div>
      </div>
    );
  }

}
