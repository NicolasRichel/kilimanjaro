// Styles
import "./ActionButton.scss";

export default function ActionButton(props) {
  const styles = Object.assign(
    // Default styles
    {
      margin: "0",
      size: "28",
      color: "#EEE",
      textColor: "#000",
      icon: "question-circle"
    },
    props
  );

  return (
    <div
      className={`ActionButton ${props.className}`}
      onClick={props.onClick}
      style={{
        margin: styles.margin,
        minWidth: `${styles.size}px`,
        height: `${styles.size}px`,
        fontSize: `${styles.size/2}px`,
        backgroundColor: styles.color,
        color: styles.textColor
      }}
    >
      <i className={`fas fa-${styles.icon}`}></i>
      <span>{props.text}</span>
    </div>
  );
};
