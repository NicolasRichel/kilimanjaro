import React from "react";

// Styles
import "./DateInput.scss";

export default class DateInput extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      value: props.value || ""
    };
  }

  componentDidUpdate() {
    const value = this.props.value || "";
    if (value !== this.state.value) {
      this.setState({ value });
    }
  }

  setValue = (e) => {
    const date = e.target.value;
    this.setState({ value: date });
    this.props.onChange?.(date);
  };

  render() {
    return (
      <div className="DateInput">
        <input
          type="date"
          value={this.state.value}
          onChange={this.setValue}
        />
      </div>
    );
  }

}
