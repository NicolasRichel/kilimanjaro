import React from "react";

// Styles
import "./StringInput.scss";

export default class StringInput extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      value: props.value || ""
    };
  }

  componentDidUpdate() {
    const value = this.props.value || "";
    if (value !== this.state.value) {
      this.setState({ value });
    }
  }

  setValue = (e) => {
    const text = e.target.value;
    this.setState({ value: text });
    this.props.onChange?.(text);
  };

  render() {
    return (
      <div className={`StringInput ${this.props.className}`}>
        <input
          type="text"
          placeholder={this.props.placeholder}
          value={this.state.value}
          onChange={this.setValue}
        />
      </div>
    );
  }

}
