// Styles
import "./ToolbarButton.scss";

export default function ToolbarButton(props) {
  return (
    <div
      className="ToolbarButton"
      title={props.title}
      onClick={props.onClick}
    >
      <i className={`fas fa-${props.icon}`}></i>
    </div>
  );
}
