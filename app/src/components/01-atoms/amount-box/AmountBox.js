// Styles
import "./AmountBox.scss";

export default function AmountBox({ amount }) {
  return (
    <span className={`AmountBox ${amount < 0 ? "negative" : "positive"}`}>
      {(amount > 0 ? "+" : "") + amount}
    </span> 
  );
}
