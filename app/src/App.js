import { getLocalDate, getFirstDayOfMonth } from "@nrl/js-commons";
import React from "react";
import { LOAD_LABELS, SET_DATE_RANGE, SET_PERIOD } from "@/state/actions.js";
import { dispatch, subscribe } from "@/state/store.js";

// Components
import DialogViewport from "@organisms/dialog-viewport/DialogViewport.js";
import NotificationViewport from "@organisms/notification-viewport/NotificationViewport.js";
import OperationsManager from "@organisms/operations-manager/OperationsManager.js";
import StatisticsManager from "@organisms/statistics-manager/StatisticsManager.js";
import Timeline from "@organisms/timeline/Timeline.js";
import Toolbar from "@organisms/toolbar/Toolbar.js";

// Styles
import "@/styles/layout.scss";

export default class App extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      labels: [],
      operations: [],
      dateRange: []
    };
  }

  componentDidMount() {
    const { unsubscribe } = subscribe(
      ["labels", "operations", "dateRange"],
      state => this.setState({
        labels: state.labels,
        dateRange: state.dateRange,
        operations: state.operations
          .filter(op => op.date >= state.dateRange[0] && op.date <= state.dateRange[1])
          .sort((o1, o2) => o1.date < o2.date),
      })
    );
    this.unsubscribe = unsubscribe;

    dispatch(LOAD_LABELS);

    const today = getLocalDate();
    dispatch(SET_PERIOD, { period: ["2020-01-01", today] });
    dispatch(SET_DATE_RANGE, { dateRange: [getFirstDayOfMonth(today), today] });
  }

  componentWillUnmount() {
    this.unsubscribe();
  }

  render() {
    return (
      <div className="App">
        <div className="App__header">
          <h1>Kilimanjaro</h1>
        </div>
        <div className="App__body">
          <div className="App__body__left-container">
            <Toolbar />
          </div>
          <div className="App__body__main-container">
            <OperationsManager
              labels={this.state.labels}
              operations={this.state.operations}
              dateRange={this.state.dateRange}
            />
            <StatisticsManager
              labels={this.state.labels}
              operations={this.state.operations}
              dateRange={this.state.dateRange}
            />
          </div>
          <div className="App__body__right-container">
            <Timeline />
          </div>
        </div>
        <NotificationViewport />
        <DialogViewport />
      </div>
    );
  }

}
