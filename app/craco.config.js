const { CracoAliasPlugin: CracoAlias } = require('react-app-alias');
const CracoSassResourcesLoader = require('craco-sass-resources-loader');

module.exports = {
  plugins: [
    {
      plugin: CracoAlias,
    },
    {
      plugin: CracoSassResourcesLoader,
      options: {
        resources: [
          './src/styles/variables.scss',
        ]
      }
    }
  ]
};
